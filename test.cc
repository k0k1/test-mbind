#include <cstdio>
#include <cstdlib>
#include <cinttypes>
#include <x86intrin.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <numaif.h>

class child_process {
public:
	struct callback {
		int (*func)(void *);
		void *arg;
	};

	child_process(callback& cb) : pid_{-1} {
		cb_ = new callback;
		*cb_ = cb;
	}

	int create() {
		pid_t pid {fork()};
		if (pid < 0)
			perror("fork");
		else if (pid == 0) {
			if (cofigure_numa_memory_policy() < 0)
				return -1;
			exit((*(cb_->func))(cb_->arg));
		}

		pid_ = pid;
		return 0;
	}

	int wait() {
		int child_result {-1};

		waitpid(pid_, &child_result, 0);
		if (child_result < 0)
			puts("cofigure_numa_memory_policy failed.");

		return 0;
	}

private:
	const int NB_CPUS {4};
	callback* cb_;
	pid_t pid_;

	int cofigure_numa_memory_policy() {
		int old_mode {-1};
		unsigned long nmask {1UL << (__rdtsc() % NB_CPUS)};
		unsigned long maxnode {sizeof nmask * 8};

		if (get_mempolicy(&old_mode, NULL, 0, NULL, 0) < 0)
			perror("get_mempolicy");

		if (set_mempolicy(MPOL_BIND, &nmask, maxnode) < 0) {
			perror("set_mempolicy");
			return -1;
		}

		return 0;
	}
};

class callback {
public:
	static int handler(void *arg) {
		(void) arg;

		uint8_t* p = allocate_hugepage();

		for (int i {0}; i < 16; i++)
			p[i] = i;

		sleep(3600);
		return 0;
	}

private:
	static uint8_t* allocate_hugepage() {
		const char *path = "/dev/hugepages/test";
		if (unlink(path) < 0)
			perror("unlink");
		int fd = open(path, O_CREAT | O_RDWR, 0666);
		if (fd < 0)
			perror("open");

		uint8_t *p {
			static_cast<uint8_t*>(
				mmap(NULL, 2 * 1024 * 1024, PROT_READ | PROT_WRITE,
				     MAP_SHARED, fd, 0)
			)
		};

		if (p == MAP_FAILED) {
			perror("mmap");
			p = nullptr;
		}
		return p;
	}
};

int
main()
{
	child_process::callback callback = {
		.func = callback::handler,
		.arg = nullptr,
	};

	child_process child(callback);
	child.create();
	child.wait();

	return 0;
}
