SRCS := test.cc
TARGET := test
CXXFLAGS += -Wall -ggdb3 -O3 -std=gnu++11

.PHONY: all clean
all: $(TARGET)
$(TARGET): $(SRCS)
	$(CXX) -o $@ $(CXXFLAGS) $< -lnuma
clean:
	$(RM) $(TARGET)
